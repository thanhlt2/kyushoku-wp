<?php
if(!is_page_template('register.php')):
?>
<div class="space_footer"></div>
<?php
endif;
?>
<div id="footer">
    <div class="container">

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 info-col">
            <h1>株式会社エボラブルアジアエージェント</h1>
            <h2>Evolable Asia Agent Co., Ltd.</h2>
            <h4>Address</h4>
            <p>
                〒105-0012</br>
                東京都港区芝大門1-10-11　芝大門センタービル10F</br>
                10F Shiba Daimon Centre, 1-10-11 Shiba Daimon, Minato Ku, </br>
                Tokyo, Japan</br>
                Zip: 105-0012</br>
                <b>Tel:</b> 03-6316-4865 - <b>Fax:</b> 03-6880-9201
            </p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 fb-col">
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=124639054226284";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-page" data-href="https://www.facebook.com/EvolableAsiaAgent" data-width="444" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
                <div class="fb-xfbml-parse-ignore">
                    <blockquote cite="https://www.facebook.com/EvolableAsiaAgent">
                        <a href="https://www.facebook.com/EvolableAsiaAgent">Work In Japan-Consulting Service＜就職情報＞株式会社エボラブルアジアエージェント</a>
                    </blockquote>
                </div>
            </div>                    
        </div>
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>
