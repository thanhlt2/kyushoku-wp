<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">

        <link type="image/x-icon" href="<?php echo get_template_directory_uri() ?>/images/evaicon.png" rel="shortcut icon">

        <meta content="Evolable Asia Agent Co.,Ltd." name="author">
        <meta content="IT/Web業界の人材やグローバル人材の紹介ならエバディ。" itemprop="title" name="title">
        <meta content="国内外の優秀な人材をご紹介いたします。御社の成長戦略実現をサポートさせていただくパートナーとして、単なる人材の紹介に留まらず、採用計画・人材配置・外国人採用定着のノウハウなど、人事戦略全般についてご相談を承ります。" itemprop="description" name="description">
        <meta content="転職、求人、中途採用、ITエンジニア、Webエンジニア、グローバル人材、外国人採用、java、php、エバディ、Evolable Asia Agent" itemprop="keywords" name="keywords">

        <title><?php echo wp_title('｜') ?></title>
        
        <!-- Bootstrap -->
        <link href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri() ?>/css/style.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <script src="<?php echo get_template_directory_uri() ?>/js/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/js/jquery-ui.js"></script>
        
        <?php
        wp_head();
        ?>
    </head>
    <body>
        <div id="header">
            <h1>IT/Web業界の人材やグローバル人材の紹介ならエバディ</h1>
            <div class="container">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" >
                    <a href="<?php echo bloginfo('url') ?>"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/image/logo.png" width="263"></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" >
                    <p><a href="http://agent.evolable.asia">採用をご検討中の企業様へ</a></p>
                </div> 
            </div>
        </div>