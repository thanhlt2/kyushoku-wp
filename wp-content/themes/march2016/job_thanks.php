<?php
/*
 * Author: haudv
 * Template Name: Job Thanks
 * 
 */
get_header();
?>
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="apply-content clearfix center">
            <h2 class="center">お申し込みありがとうございます。エントリーが完了しました。</h2>                    
            <p style="margin:20px 0;font-size:14px;">
                お申し込みありがとうございます<br/>
                エントリーが完了いたしました。<br/><br/>
                弊社で確認後、折り返しご連絡させていただきます。<br/><br/>
                上記フォームで送信できない場合は、必要項目をご記入の上、<br />
                <a href="mailto:info@agent.evolable.asia" class="link">info@agent.evolable.asia</a>までメールをお送りください。
            </p> 
            <br>
            <a href="<?php echo bloginfo('url') ?>" class="link-top">Topへ戻る</a>
        </div>
    </div>
</div>
<?php get_footer(); ?>