<?php
/*
 * Author: haudv
 * Template Name: Job Corfirm
 * 
 */
if (!defined('ABSPATH')) {
    die('No script kiddies please!');
}
if (!function_exists('wp_handle_upload')) {
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
}

function wp_jobs_upload_dir($dirs) {
    $y = date('Y');
    $m = date('m');

    $dirs['subdir'] = "/cv/{$y}/{$m}";
    $dirs['path'] = $dirs['basedir'] . "/cv/{$y}/{$m}";
    $dirs['url'] = $dirs['baseurl'] . "/cv/{$y}/{$m}";

    return $dirs;
}
function my_mail_content_type($content_type) {
    return 'text/html';
}

add_filter('wp_mail_content_type', 'my_mail_content_type');
if (isset($_POST['confirm'])) {
    $job_id = $_POST['job_id'];
    $job = get_post($job_id);
    $cjob = get_post_custom($job_id);
    
    $name = @htmlspecialchars(stripslashes($_POST['canname']));
    $phonetic_name = @htmlspecialchars(stripslashes($_POST['phonetic_name']));
    $phone_number = @htmlspecialchars(stripslashes($_POST['phone_number']));
    $email = @htmlspecialchars(stripslashes($_POST['email']));
    $cv_file = $_POST['cv_file'];
    $resume_file = $_POST['resume_file'];
    $other_file = $_POST['other_file'];
    $expected_salary = @htmlspecialchars(stripslashes($_POST['expected_salary']));
    $request = @htmlspecialchars(stripslashes($_POST['request']));
    $reg_inq = @$_POST['inq'];
    $reg_inq_other = @htmlspecialchars(stripslashes($_POST['inq_other']));
    
    $reg_inq_all = NULL;
    $arr_inq = array('エバディHPから', '検索エンジンで検索', 'Twitter・Facebookなどのソーシャルネットワークから', '海外就職情報サイトから', 'アセナビから', 'その他');
    $arr_inq_checked = array('', '', '', '', '', '');
    if (is_array($reg_inq)) {
        foreach ($reg_inq as $value) {
            $key = array_search($value, $arr_inq);
            $arr_inq_checked[$key] = 'checked';
            //
            if ($value != 'その他') {
                $reg_inq_all[] = htmlspecialchars($value);
            } else {
                if ($reg_inq_other != '') {
                    $reg_inq_all[] = htmlspecialchars($reg_inq_other);
                }                
            }
        }
    }
    
    $_SESSION['candidate'] = array(
        'name' => $name,
        'phonetic_name' => $phonetic_name,
        'phone_number' => $phone_number,
        'email' => $email,
        //'cv_file'=>$cv_file,
        //'resume_file'=>$resume_file,
        //'other_file'=>$other_file,
        'expected_salary' => $expected_salary,
        'request' => $request,
        'job_id' => $job_id,
        'job_no' => $cjob['job_no'][0],
        'job_title' => $job->post_title,
        'inq' => $reg_inq,
        'inq_other' => $reg_inq_other,
        'inq_all' => $reg_inq_all,
    );
    $file_exts = array('pdf','doc','docx','xls','xlsx','zip','rar');
    add_filter('upload_dir', 'wp_jobs_upload_dir');
    foreach ($_FILES as $key => $value) {
        if ($value['size'] > 0) {            
            $filetype = wp_check_filetype($value['name']);
            if(!in_array($filetype['ext'], $file_exts)){
                unset($_SESSION['file_errors']);
                $_SESSION['file_errors'][$key] = 'File extension must be '.  implode(',', $file_exts);
                wp_redirect(home_url() . '/job-apply/'.$job_id);
                exit();
            }            
            
        }else if(!empty($value[name])){
            unset($_SESSION['file_errors']);
            $_SESSION['file_errors'][$key] = 'File size is too big';
            wp_redirect(home_url() . '/job-apply/'.$job_id);  
            exit();
        }
        
        $_SESSION['files'][$key] = $value;
    }
    
    $_SESSION['files'] = $_FILES;
    $upload_overrides = array('test_form' => false);
    foreach ($_SESSION['files'] as $key => $value) {
        if ($value['size'] > 0) {
            $movefile = wp_handle_upload($value, $upload_overrides);

            $url_file = '';
            if ($movefile && !isset($movefile['error'])) {
                chmod($movefile['file'], 444);
                $url_file = isset($movefile['url']) ? $movefile['url'] : '';
                $_SESSION['candidate'][$key] = $url_file;
            } else {
                $result = array(
                    'code' => 'ERR',
                    'message' => $movefile['error'],
                );
                var_dump($result);
                exit;
            }
        }
    }
    remove_filter('upload_dir', 'wp_jobs_upload_dir');
} elseif (isset($_POST['thanks'])) {
    //validate once again
    $table_name = $wpdb->prefix . 'job_candidates';

    //$download_link = isset($_POST['job_id']) ? (home_url() . '/download?attach=' . $_POST['job_id']) : '';
    $data = $_SESSION['candidate'];
    $data['apply_date'] = current_time('mysql');

    try {
        $data_import = array();
        foreach ($data as $key => $value) {
            if ($key == 'inq_all') {
                $inquiry = '';
                if (is_array($value)) {
                    $inquiry = implode(',', $value);
                }
                $data_import['inquiry'] = $inquiry;
            } else {
                $arr_db = array ('name', 'phonetic_name', 'phone_number', 'email', 'expected_salary', 'resume_file', 'cv_file' , 'other_file','request', 'apply_date', 'job_id', 'job_no', 'job_title');
                if (in_array($key, $arr_db)) {
                    $data_import[$key] = $value;
                }
            }
        }
        $wpdb->insert($table_name, $data_import);
        $last_id = $wpdb->insert_id;

        if ($last_id > 0) {
            array_merge($data, array(
                'id' => $last_id
            ));
            $job_id = $data['job_id'];
            $term_locations = get_the_terms($job_id, 'job-location');
            $term_positions = get_the_terms($job_id, 'job-position');
            $term_skills = get_the_terms($job_id, 'job-skill');
            $locations = array();
            $positions = array();
            $skills = array();
            foreach ($term_locations as $key => $value) {
                $locations[] = $value->name;
            }
            foreach ($term_positions as $key => $value) {
                $positions[] = $value->name;
            }
            foreach ($term_skills as $key => $value) {
                $skills[] = $value->name;
            }
            
            $data['job_location'] = implode(",", $locations);
            $data['job_position'] = implode(",", $positions);
            $data['job_skill'] = implode(",", $skills);
            /* -------------------------------------------------------------- send mail */
            require_once (dirname(__FILE__) . '/lib/includes/Twig/Autoloader.php');
            Twig_Autoloader::register();

            $loader = new Twig_Loader_String;

            $twig = new Twig_Environment($loader);

            $from = job_get_option('wpt_job_text_from_email');
            $fromname = job_get_option('wpt_job_text_from_name');

            // Mail to Candidate
            $body_candidate = job_get_option('wpt_job_text_block_candidate');
            if (isset($body_candidate) && $body_candidate != '') {
                $body_candidate = $twig->render($body_candidate, $data);   
                
                //
                $subject_candidate = $twig->render(job_get_option('wpt_job_text_subject_candidate'), $data);
                //
                $headers = "From: " . $fromname . " <" . $from . ">\r\n";
                //	
                wp_mail($data['email'], stripslashes($subject_candidate), stripslashes($body_candidate));
            }

            //Admin用メッセージ
            $body_admin = job_get_option('wpt_job_text_block_admin');
            if (isset($body_admin) && $body_admin != '') {
                $body_admin = $twig->render($body_admin, array_merge(
                                $data, array(
                    'entry_time' => gmdate("Y/m/d H:i:s", time() + 9 * 3600),
                    'entry_host' => gethostbyaddr(getenv("REMOTE_ADDR")),
                    'entry_ua' => getenv("HTTP_USER_AGENT"),
                )));
                //
                $subject_admin = job_get_option('wpt_job_text_subject_admin');
                //
                $list_email = job_get_option('wpt_job_text_list_email');
                if (isset($list_email) && $list_email != '') {
                    $list_email = preg_split('/\r\n|\n|\r/', $list_email);
                    //
                    $headers = "From: " . $fromname . " <" . $from . ">\r\n";
                    //
                    $attachments = array();
                    foreach ($_SESSION['candidate'] as $key => $value) {
                        if(in_array($key, array('cv_file','resume_file','other_file'))){
                            // attach file into mail
                            $attach_file = $value;
                            //
                            $path = str_replace(home_url(), '', $attach_file);
                            $attach_file_path = WP_CONTENT_DIR . str_replace('/wp-content', '', $path);
                            $attachments[] = $attach_file_path;
                        }
                    }
                    //
                    wp_mail($list_email, stripslashes($subject_admin), stripslashes($body_admin), '', $attachments);
                }
            }
        }
    } catch (Exception $ex) {
        //var_dump($ex);
    }
    unset($_SESSION['candidate']);
    unset($_SESSION['files']);
    unset($_SESSION['file_errors']);

    wp_redirect(home_url() . '/job-thanks');
} else {
    wp_redirect(home_url());
}

get_header();
?>
<style>
    .apply-content ul.a li {
        display:list-item;
    }
    .apply-content ul.a {
        list-style-type: square;        
    }
</style>
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="apply-content clearfix">
            <h2>ご入力内容の確認</h2>      
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding-lr">
                <div class="description">
                    以下の内容がメールで送信されます
                    <div class="space"></div>
                    入力・選択された内容をご確認のうえ、「入力内容を送信」ボタンをクリックしてください。入力内容を訂正する場合は、「戻る」をクリックしてください。
                </div>
            </div>
            <div class="clearfix"></div>
            <form method="post" enctype="multipart/form-data">
                <div class="apply-content-table-border">
                    <div class="apply-content-table">
                        <div class="apply-content-row">
                            <div class="apply-content-cell-1">
                                <h4>お名前/NAME</h4>
                            </div>
                            <div class="apply-content-cell-2">
                                <?php echo $_SESSION['candidate']['name'] ?>
                            </div>
                        </div>         
                        <div class="apply-content-row">
                            <div class="apply-content-cell-1">
                                <h4>氏名(フリガナ).</h4>
                            </div>
                            <div class="apply-content-cell-2">
                                <?php echo $_SESSION['candidate']['phonetic_name'] ?>
                            </div>
                        </div> 
                        <div class="apply-content-row">
                            <div class="apply-content-cell-1">
                                <h4>電話番号／Phone Number</h4>
                            </div>
                            <div class="apply-content-cell-2">
                                <?php echo $_SESSION['candidate']['phone_number'] ?>
                            </div>
                        </div> 
                        <div class="apply-content-row">
                            <div class="apply-content-cell-1">
                                <h4>メールアドレス／e-mail address</h4>
                            </div>
                            <div class="apply-content-cell-2">
                                <?php echo $_SESSION['candidate']['email'] ?>
                            </div>
                        </div>  
                        <div class="apply-content-row">
                            <div class="apply-content-cell-1">
                                <h4>履歴書</h4>
                            </div>
                            <div class="apply-content-cell-2">
                                <?php echo $_SESSION['files']['resume_file']['name'] ?>
                            </div>
                        </div>   
                        <div class="apply-content-row">
                            <div class="apply-content-cell-1">
                                <h4>職務経歴書</h4>
                            </div>
                            <div class="apply-content-cell-2">
                                <?php echo $_SESSION['files']['cv_file']['name'] ?>
                            </div>
                        </div>
                        <div class="apply-content-row">
                            <div class="apply-content-cell-1">
                                <h4>その他 添付ファイル</h4>
                            </div>
                            <div class="apply-content-cell-2">
                                <?php echo $_SESSION['files']['other_file']['name'] ?>
                            </div>
                        </div>
                        <div class="apply-content-row">
                            <div class="apply-content-cell-1">
                                <h4>希望年収 [万円]</h4>
                            </div>
                            <div class="apply-content-cell-2">
                                <?php echo $_SESSION['candidate']['expected_salary'] ?>
                            </div>
                        </div>
                        <div class="apply-content-row">
                            <div class="apply-content-cell-1">
                                <h4>その他ご要望.</h4>
                            </div>
                            <div class="apply-content-cell-2">
                                <?php echo nl2br($_SESSION['candidate']['request']) ?>
                            </div>
                        </div> 
                        <div class="apply-content-row">
                            <div class="apply-content-cell-1">
                                <h4>どこで求人を知りましたか?</h4>
                            </div>
                            <div class="apply-content-cell-2">
                            <?php if (!is_null($_SESSION['candidate']['inq_all'])): ?>
                                <ul class="a">
                            <?php foreach ($_SESSION['candidate']['inq_all'] as $value): ?>
                                <li><?php echo $value ?></li>
                            <?php endforeach; ?>
                            </ul>
                            <?php endif; ?>
                            </div>
                        </div> 
                    </div>                        
                </div>
                <p class="applyjob"><button type="submit">エントリー</button></p>
                <input type="hidden" name="thanks" value="thanks"/>
            </form>
        </div>
    </div>
</div>
<?php get_footer(); ?>