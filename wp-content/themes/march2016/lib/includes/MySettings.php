<?php
/**
 * Tabbed Settings Page
 */
//add_action('init', 'ilc_admin_init');
add_action('admin_menu', 'ilc_settings_page_init');

//
//function ilc_admin_init() {
//    $settings = get_option("my_theme_option");
//    if (empty($settings)) {
//        $settings = array(
//            'ilc_intro' => 'Some intro text for the home page',
//            'ilc_tag_class' => false,
//            'ilc_ga' => false
//        );
//        add_option("my_theme_option", $settings, '', 'yes');
//    }
//}

function ilc_settings_page_init() {
    $theme_data = get_theme_data(TEMPLATEPATH . '/style.css');
    $settings_page = add_theme_page($theme_data['Name'] . ' Theme Settings', $theme_data['Name'] . ' Theme Settings', 'edit_theme_options', 'theme-settings', 'ilc_settings_page');
    add_action("load-{$settings_page}", 'ilc_load_settings_page');
}

function ilc_load_settings_page() {
    if ($_POST["ilc-settings-submit"] == 'Y') {
        check_admin_referer("ilc-settings-page");
        ilc_save_theme_settings();
        $url_parameters = isset($_GET['tab']) ? 'updated=true&tab=' . $_GET['tab'] : 'updated=true';
        wp_redirect(admin_url('themes.php?page=theme-settings&' . $url_parameters));
        exit;
    }
}

function ilc_save_theme_settings() {
    global $pagenow;
    $settings = get_option("my_theme_option");
    if ($pagenow == 'themes.php' && $_GET['page'] == 'theme-settings') {
        if (isset($_GET['tab']))
            $tab = $_GET['tab'];
        else
            $tab = 'general';

        switch ($tab) {
            case 'general' :
                $settings['ct_benchmark_script'] = $_POST['ct_benchmark_script'];
                break;            
        }
    }

//    if (!current_user_can('unfiltered_html')) {
//        if ($settings['ct_google_analytics'])
//            $settings['ct_google_analytics'] = stripslashes(esc_textarea(wp_filter_post_kses($settings['ct_google_analytics'])));
//        if ($settings['ct_google_tag_manager'])
//            $settings['ct_google_tag_manager'] = stripslashes(esc_textarea(wp_filter_post_kses($settings['ct_google_tag_manager'])));
//    }

    $updated = update_option("my_theme_option", $settings);
}

function ilc_admin_tabs($current = 'general') {
    $tabs = array('general' => 'General');
    $links = array();
    echo '<div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
    foreach ($tabs as $tab => $name) {
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?page=theme-settings&tab=$tab'>$name</a>";
    }
    echo '</h2>';
}

function ilc_settings_page() {
    global $pagenow;
    $settings = get_option("my_theme_option");
    $theme_data = wp_get_theme();
    ?>

    <div class="wrap">
        <h2><?php echo $theme_data['Name']; ?> Theme Settings</h2>

        <?php
        if ('true' == esc_attr($_GET['updated']))
            echo '<div class="updated" ><p>Theme Settings updated.</p></div>';

        if (isset($_GET['tab']))
            ilc_admin_tabs($_GET['tab']);
        else
            ilc_admin_tabs('general');
        ?>

        <div id="poststuff">
            <form method="post" action="<?php admin_url('themes.php?page=theme-settings'); ?>">
                <?php
                wp_nonce_field("ilc-settings-page");

                if ($pagenow == 'themes.php' && $_GET['page'] == 'theme-settings') {

                    if (isset($_GET['tab']))
                        $tab = $_GET['tab'];
                    else
                        $tab = 'general';

                    echo '<table class="form-table">';
                    switch ($tab) {
                        case 'general' :
                            ?>                            
                            <tr>
                                <th><label for="ct_benchmark_script">Benchmark Javascript Code:</label></th>
                                <td>
                                    <textarea id="ct_benchmark_script" name="ct_benchmark_script" cols="60" rows="5"><?php echo esc_html(stripslashes($settings["ct_benchmark_script"])); ?></textarea><br/>
                                </td>
                            </tr>
                            <?php
                            break;                        
                    }
                    echo '</table>';
                }
                ?>
                <p class="submit" style="clear: both;">
                    <input type="submit" name="Submit"  class="button-primary" value="Update Settings" />
                    <input type="hidden" name="ilc-settings-submit" value="Y" />
                </p>
            </form>

            <p><?php echo $theme_data['Name'] ?> theme by <?php echo $theme_data['Author'] ?> (<?php echo $theme_data['Version'] ?>)</p>
        </div>

    </div>
    <?php
}
?>