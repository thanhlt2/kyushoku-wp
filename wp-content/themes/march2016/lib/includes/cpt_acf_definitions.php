<?php

/*
 * This file is custom post type, custom taxonomy and custom fields
 * definition file.
 * 
 * Exported from CPT UI & Advanced Custom Fields
 */

/* ---------------------------------------------------------------------------- */
/* post type definitions */
/* ---------------------------------------------------------------------------- */
add_action('init', 'cptui_register_my_cpts');
add_action('init', 'cptui_register_my_taxes');

function cptui_register_my_cpts() {
    $labels = array(
        "name" => "Jobs",
        "singular_name" => "Jobs",
    );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "jobs", "with_front" => true),
        "query_var" => true,
        "menu_position" => 32,
        "menu_icon" => get_template_directory_uri() . '/image/ad-ico/h7.png',
        "supports" => array("title"),
    );
    register_post_type("jobs", $args);

    $labels = array(
        "name" => "Banner",
        "singular_name" => "Banner",
    );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "banner", "with_front" => true),
        "query_var" => true,
        "supports" => array("title"),
    );
    register_post_type("banner", $args);

    /**
     * @author HangVo
     * @date: 2016/04/01
     * @functuion: add post-type: news
     */
    $labels = array(
        "name" => "News",
        "singular_name" => "News",
    );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "news", "with_front" => true),
        "query_var" => true,
        "supports" => array("title"),
    );
    register_post_type("news", $args);
// End of cptui_register_my_cpts()
}

/* ---------------------------------------------------------------------------- */
/* taxonomy definitions */
/* ---------------------------------------------------------------------------- */

function cptui_register_my_taxes() {
    $labels = array(
        "name" => "Job Locations",
        "label" => "Job Locations",
    );

    $args = array(
        "labels" => $labels,
        "hierarchical" => true,
        "label" => "Job Locations",
        "show_ui" => true,
        "query_var" => true,
        "rewrite" => array('slug' => 'job-location', 'with_front' => true),
        "show_admin_column" => false,
    );
    register_taxonomy("job-location", array("jobs"), $args);

    $labels = array(
        "name" => "Job Positions",
        "label" => "Job Positions",
    );

    $args = array(
        "labels" => $labels,
        "hierarchical" => true,
        "label" => "Job Positions",
        "show_ui" => true,
        "query_var" => true,
        "rewrite" => array('slug' => 'job-position', 'with_front' => true),
        "show_admin_column" => false,
    );
    register_taxonomy("job-position", array("jobs"), $args);

    $labels = array(
        "name" => "Job Skills",
        "label" => "Job Skills",
    );

    $args = array(
        "labels" => $labels,
        "hierarchical" => true,
        "label" => "Job Skills",
        "show_ui" => true,
        "query_var" => true,
        "rewrite" => array('slug' => 'job-skill', 'with_front' => true),
        "show_admin_column" => false,
    );
    register_taxonomy("job-skill", array("jobs"), $args);
}

/* ---------------------------------------------------------------------------- */
/* custom fields definitions */
/* ---------------------------------------------------------------------------- */
if (function_exists("register_field_group")) {

    register_field_group(array(
        'id' => 'acf_jobs',
        'title' => 'Jobs',
        'fields' => array(
            array (
                    'key' => 'field_5701c4e0ca8a9',
                    'label' => 'Sub Title',
                    'name' => 'sub_title',
                    'type' => 'text',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => '',
            ),
            array (
                    'key' => 'field_5701c556ca8aa',
                    'label' => '求人No',
                    'name' => 'job_no',
                    'type' => 'text',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => '',
            ),
            array (
                    'key' => 'field_5701c57fca8ab',
                    'label' => 'Min Salary',
                    'name' => 'min_salary',
                    'type' => 'text',
                    'default_value' => 0,
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '万円',
                    'formatting' => 'none',
                    'maxlength' => '',
            ),
            array (
                    'key' => 'field_5701c63fca8ad',
                    'label' => 'Default Image',
                    'name' => 'default_image',
                    'type' => 'image',
                    'save_format' => 'url',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
            ),
            array (
                    'key' => 'field_5704778d8d061',
                    'label' => 'Detail Image',
                    'name' => 'detail_image',
                    'type' => 'image',
                    'save_format' => 'url',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
            ),
            array (
                    'key' => 'field_5701c6a3ca8ae',
                    'label' => '事業内容',
                    'name' => 'content',
                    'type' => 'textarea',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => 2,
                    'formatting' => 'br',
            ),
            array (
                    'key' => 'field_5701c6a5ca8af',
                    'label' => '仕事内容',
                    'name' => 'detail',
                    'type' => 'textarea',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => 2,
                    'formatting' => 'br',
            ),
            array (
                    'key' => 'field_5701c6a7ca8b0',
                    'label' => '必要経験',
                    'name' => 'experience',
                    'type' => 'textarea',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => 2,
                    'formatting' => 'br',
            ),
            array (
                    'key' => 'field_570339828af55',
                    'label' => '想定給与',
                    'name' => 'salary',
                    'type' => 'textarea',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => 2,
                    'formatting' => 'br',
            ),
            array (
                    'key' => 'field_570339e48af56',
                    'label' => '勤務地',
                    'name' => 'location',
                    'type' => 'textarea',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => 2,
                    'formatting' => 'br',
            ),
            array (
                    'key' => 'field_57033a108af57',
                    'label' => 'Description',
                    'name' => 'description',
                    'type' => 'wysiwyg',
                    'default_value' => '',
                    'toolbar' => 'full',
                    'media_upload' => 'yes',
            ),
            array(
                'key' => 'field_568c90bdd320c',
                'label' => 'Block Info',
                'name' => 'block_info',
                'type' => 'repeater',
                'sub_fields' => array(
                    array(
                        'key' => 'field_568c98d5d320d',
                        'label' => 'TItle',
                        'name' => 'title',
                        'type' => 'select',
                        'column_width' => '',
                        'choices' => array(
                            '企業・求人概要' => '企業・求人概要',
                            '応募条件' => '応募条件',
                            '勤務・就業規定・その他情報' => '勤務・就業規定・その他情報',
                            '募集要項' => '募集要項',
                        ),
                        'default_value' => '',
                        'allow_null' => 0,
                        'multiple' => 0,
                    ),
                    array(
                        'key' => 'field_568c9c44d320e',
                        'label' => 'Description 1',
                        'name' => 'description_1',
                        'type' => 'repeater',
                        'conditional_logic' => array(
                            'status' => 1,
                            'rules' => array(
                                array(
                                    'field' => 'field_568c98d5d320d',
                                    'operator' => '==',
                                    'value' => '企業・求人概要',
                                ),
                            ),
                            'allorany' => 'all',
                        ),
                        'column_width' => '',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_568c9d72d320f',
                                'label' => 'Sub Title',
                                'name' => 'sub_title',
                                'type' => 'select',
                                'column_width' => '',
                                'choices' => array(
                                    '募集背景' => '募集背景',
                                    '概要' => '概要',
                                ),
                                'default_value' => '',
                                'allow_null' => 0,
                                'multiple' => 0,
                            ),
                            array(
                                'key' => 'field_568cabe0d3210',
                                'label' => 'Content',
                                'name' => 'content',
                                'type' => 'textarea',
                                'column_width' => '',
                                'default_value' => '',
                                'placeholder' => '',
                                'maxlength' => '',
                                'rows' => '',
                                'formatting' => 'br',
                            ),
                        ),
                        'row_min' => '',
                        'row_limit' => '',
                        'layout' => 'table',
                        'button_label' => 'Add Row',
                    ),
                    array(
                        'key' => 'field_568cac3fd3211',
                        'label' => 'Description 2',
                        'name' => 'description_2',
                        'type' => 'repeater',
                        'conditional_logic' => array(
                            'status' => 1,
                            'rules' => array(
                                array(
                                    'field' => 'field_568c98d5d320d',
                                    'operator' => '==',
                                    'value' => '応募条件',
                                ),
                            ),
                            'allorany' => 'all',
                        ),
                        'column_width' => '',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_568cac51d3212',
                                'label' => 'Sub Title',
                                'name' => 'sub_title',
                                'type' => 'select',
                                'column_width' => '',
                                'choices' => array(
                                    '応募資格' => '応募資格',
                                    '雇用区分' => '雇用区分',
                                    '想定年収(給与詳細)' => '想定年収(給与詳細)',
                                    '採用人数' => '採用人数',
                                    '選考プロセス' => '選考プロセス',
                                ),
                                'default_value' => '',
                                'allow_null' => 0,
                                'multiple' => 0,
                            ),
                            array(
                                'key' => 'field_568cac83d3213',
                                'label' => 'Content',
                                'name' => 'content',
                                'type' => 'textarea',
                                'column_width' => '',
                                'default_value' => '',
                                'placeholder' => '',
                                'maxlength' => '',
                                'rows' => '',
                                'formatting' => 'br',
                            ),
                        ),
                        'row_min' => '',
                        'row_limit' => '',
                        'layout' => 'table',
                        'button_label' => 'Add Row',
                    ),
                    array(
                        'key' => 'field_568caca9d3214',
                        'label' => 'Description 3',
                        'name' => 'description_3',
                        'type' => 'repeater',
                        'conditional_logic' => array(
                            'status' => 1,
                            'rules' => array(
                                array(
                                    'field' => 'field_568c98d5d320d',
                                    'operator' => '==',
                                    'value' => '勤務・就業規定・その他情報',
                                ),
                            ),
                            'allorany' => 'all',
                        ),
                        'column_width' => '',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_568cacbbd3215',
                                'label' => 'Sub Title',
                                'name' => 'sub_title',
                                'type' => 'select',
                                'column_width' => '',
                                'choices' => array(
                                    '勤務地' => '勤務地',
                                    '勤務時間' => '勤務時間',
                                    '待遇・福利厚生' => '待遇・福利厚生',
                                    '休日/休暇' => '休日/休暇',
                                ),
                                'default_value' => '',
                                'allow_null' => 0,
                                'multiple' => 0,
                            ),
                            array(
                                'key' => 'field_568cacd7d3216',
                                'label' => 'Content',
                                'name' => 'content',
                                'type' => 'textarea',
                                'column_width' => '',
                                'default_value' => '',
                                'placeholder' => '',
                                'maxlength' => '',
                                'rows' => '',
                                'formatting' => 'br',
                            ),
                        ),
                        'row_min' => '',
                        'row_limit' => '',
                        'layout' => 'table',
                        'button_label' => 'Add Row',
                    ),
                    array(
                        'key' => 'field_56a1b115816c3',
                        'label' => 'Description 4',
                        'name' => 'description_4',
                        'type' => 'repeater',
                        'conditional_logic' => array(
                            'status' => 1,
                            'rules' => array(
                                array(
                                    'field' => 'field_568c98d5d320d',
                                    'operator' => '==',
                                    'value' => '募集要項',
                                ),
                            ),
                            'allorany' => 'all',
                        ),
                        'column_width' => '',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_56a1b159816c4',
                                'label' => 'Sub Title',
                                'name' => 'sub_title',
                                'type' => 'select',
                                'column_width' => '',
                                'choices' => array(
                                    '仕事の内容' => '仕事の内容',
                                    '求める経験' => '求める経験',
                                    '雇用形態' => '雇用形態',
                                    '勤務地' => '勤務地',
                                    '想定年収' => '想定年収',
                                    '勤務時間' => '勤務時間',
                                    '休日/休暇' => '休日/休暇',
                                    '福利厚生' => '福利厚生',
                                    '選考プロセス' => '選考プロセス',
                                    '備考（その他情報）' => '備考（その他情報）',
                                ),
                                'default_value' => '',
                                'allow_null' => 0,
                                'multiple' => 0,
                            ),
                            array(
                                'key' => 'field_56a1b1fb816c5',
                                'label' => 'Content',
                                'name' => 'content',
                                'type' => 'textarea',
                                'column_width' => '',
                                'default_value' => '',
                                'placeholder' => '',
                                'maxlength' => '',
                                'rows' => '',
                                'formatting' => 'br',
                            ),
                        ),
                        'row_min' => '',
                        'row_limit' => '',
                        'layout' => 'table',
                        'button_label' => 'Add Row',
                    ),
                ),
                'row_min' => '',
                'row_limit' => '',
                'layout' => 'row',
                'button_label' => 'Add Block Info',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'jobs',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array(
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array(
            ),
        ),
        'menu_order' => 0,
    ));

    register_field_group(array(
        'id' => 'acf_banner',
        'title' => 'Banner',
        'fields' => array(
            array(
                'key' => 'field_56c3f59c293e0',
                'label' => 'Images',
                'name' => 'images',
                'type' => 'repeater',
                'sub_fields' => array(
                    array(
                        'key' => 'field_56c3f5b2293e1',
                        'label' => 'Image',
                        'name' => 'image',
                        'type' => 'image',
                        'column_width' => '',
                        'save_format' => 'object',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                    ),
                    array(
                        'key' => 'field_56c3f70ab5f8e',
                        'label' => 'Url',
                        'name' => 'url',
                        'type' => 'text',
                        'column_width' => '',
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'formatting' => 'none',
                        'maxlength' => '',
                    ),
                ),
                'row_min' => '',
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => 'Add Row',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'banner',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array(
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array(
            ),
        ),
        'menu_order' => 0,
    ));

    register_field_group(array(
        'id' => 'acf_news',
        'title' => 'News',
        'fields' => array(
            array(
                'key' => 'field_56fdf87feb669',
                'label' => 'Image',
                'name' => 'image',
                'type' => 'image',
                'save_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ),
            array(
                'key' => 'field_56fdf8bfeb66a',
                'label' => 'url',
                'name' => 'url',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_55da7a80ad0f8',
                'label' => 'Content',
                'name' => 'content',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'news',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array(
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array(
            ),
        ),
        'menu_order' => 0,
    ));
}    