<?php
/*
 * Author: haudv
 * Template Name: Confirm for registering
 * 
 */
get_header();
?>
<div class="center">
    
    <div class="thank-regist-header">
        ご登録頂きありがとうございます。
    </div> 
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="apply-content clearfix center m-b-120">
                <div class="confirm-regist">
                    メールアドレスの確認が完了しました。
                </div> 
                <p class="but-thanks"><a href="<?php echo home_url() ?>">トップのページにもどる</a></p>
            </div>            
        </div>        
    </div>    
</div>
<?php get_footer(); ?>