<?php
/*
 * Author: Hau Doan
 * 
 * 
 */
include_once (dirname(__FILE__) . '/lib/includes/MySettings.php');
include_once (dirname(__FILE__) . '/lib/includes/job_utility_functions.php');
include_once (dirname(__FILE__) . '/lib/includes/job_candidates.php');
include_once (dirname(__FILE__) . '/lib/includes/job_setting_mail.php');
include_once (dirname(__FILE__) . '/lib/includes/cpt_acf_definitions.php');

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
add_action('init', 'myStartSession', 1);

// init session id
function myStartSession() {
    if (!session_id()) {
        session_start();
    }
}

add_action('wp_print_scripts', 'scripts');

function scripts() {
    if (is_page('job-apply')) {
        wp_enqueue_script('js-validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array(), '1.14.0', TRUE);
        wp_enqueue_script('js-contact', get_template_directory_uri() . '/js/job.js', array('js-validate'), '1', TRUE);
    }
}

/* ------------------------------------------------------------ theme support */
global $theme_options;
$theme_options = get_option('my_theme_option');

//add_action('wp_footer', 'add_pagenavi_script');
function add_pagenavi_script() {
?>
<script type="text/javascript">
    $(function(){
        atags = $('.wp-pagenavi > a');
        num = atags.size();
        for(i = 0; i<num; i++){            
            ref = $(atags[i]).attr('href');
            if(ref.indexOf('/index/') < 0){
                ref = ref.replace('/page/','/index/page/');
                $(atags[i]).attr('href', ref);
            }            
        }
    })
</script>
<?php }


add_action('wp_head', 'add_custom_css');

function add_custom_css() {    
    global $theme_options;
    $css = '<style id="custom-css">';

    if (isset($theme_options['ct_use_css']) && $theme_options['ct_use_css']) {
        $css .= $theme_options['ct_custom_css'];
    }

    $css .= '</style>';

    echo $css;
}

/* ---------------------------------------------------------------------Title */

function set_wp_title($title, $sep) {
    global $page, $paged;

    if (is_feed()) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo('name');

    if (is_front_page() || is_home()) {
        $title = "$title";
    } else {

        if (is_page()) {
            if (is_page('job-apply')) {
                $title = "Job Apply $title";
            }

            if (is_page('job-confirm')) {
                $title = "Job Confirm $title";
            }


            if (is_page('job-thanks')) {
                $title = "Job Thanks $title";
            }


            if (is_page('register')) {
                $title = "Register $title";
            }
        }
    }
    
    return str_replace('Page not found', "", $title);
}

add_filter('wp_title', 'set_wp_title', 10, 2);