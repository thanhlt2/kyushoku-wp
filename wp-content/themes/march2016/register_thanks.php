<?php
/*
 * Author: haudv
 * Template Name: Thank you for registering
 * 
 */
get_header();
?>
<div class="center">
    
    <div class="thank-regist-header">
        ご登録頂き誠にありがとうございます。
    </div> 
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="apply-content clearfix center m-b-120">
                <div class="thank-regist">
                    本人確認用のメールを配信させて頂きました。メール内の認証リンクをクリックしてください。よろしくお願い致します。
                    <br/><br/>
                    メールが見当たらない場合は、迷惑メールフォルダに入っている可能性がございますので、ご確認ください。
                </div> 
                <p class="but-thanks"><a href="<?php echo home_url() ?>">トップのページにもどる</a></p>
            </div>            
        </div>        
    </div>    
</div>
<?php get_footer(); ?>