<?php
/*
 * Author: haudv
 * Template Name: Index
 * 
 */
get_header();
$ar_position = get_terms('job-position', array('slug'=>trim(isset($_GET['position'])?$_GET['position']:'')));
$position = count($ar_position)==1?$ar_position[0]:null;
$ar_location = get_terms('job-location', array('slug'=>trim(isset($_GET['location'])?$_GET['location']:'')));
$location = count($ar_location)==1?$ar_location[0]:null;
if(isset($location) && $location->parent > 0){
   $parent_location = get_term_by('term_id', $location->parent, 'job-location');
}
$ar_skill = get_terms('job-skill', array('slug'=>trim(isset($_GET['skill'])?$_GET['skill']:'')));
$skill = count($ar_skill)==1?$ar_skill[0]:null;
$salary = trim(isset($_GET['salary'])?$_GET['salary']:'');
$q = array();
if (!empty($location)) {
    $q[] = "location=" . $location->slug;
}
if (!empty($position)) {
    $q[] = "position=" . $position->slug;
}
if (!empty($salary)) {
    $q[] = "salary=" . $salary;
}
$q = implode("&", $q);
$args = array(
    'orderby' => 'count',
    'hide_empty' => 0
);
?>
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <ul class="bread-cum hidden-xs">
            <li><a href="<?php bloginfo('url') ?>">All Jobs</a></li>
            <?php if (!empty($location)) {
                if (!empty($parent_location)) {
                ?>
                <li class="next-icon">&gt;</li>
                <li><a href="<?php echo bloginfo('url') . "?location=" . $parent_location->slug ?>"><?php echo $parent_location->name ?></a></li>
            <?php } ?>
                <li class="next-icon">&gt;</li>
                <li><a href="<?php echo bloginfo('url') . "?location=" . $location->slug ?>"><?php echo $location->name ?></a></li>
            <?php } ?>
            <?php if (!empty($position)) { ?>
                <li class="next-icon">&gt;</li>
                <li><a href="<?php echo bloginfo('url') . "?" . $q ?>"><?php echo $position->name ?></a></li>
            <?php } ?>
            <?php if (!empty($skill)) { ?>
                <li class="next-icon">&gt;</li>
                <li><a href="<?php echo bloginfo('url') . "?".$q."&skill=" . $skill->slug ?>"><?php echo $skill->name ?></a></li>
            <?php } ?>
        </ul>
        <?php get_template_part('part_job_search') ?>    
        <div class="toolbox">求人情報</div>
        <?php if ($wp_query->have_posts()): ?>
        
            <?php while ($wp_query->have_posts()): $wp_query->the_post(); ?>
                <div class="jobbox clearfix" id="<?php the_ID() ?>">
                    <h1><?php the_title() ?></h1>
                    <span><?php echo get_field('sub_title'); ?></span>
                    <br>
                    <?php
                    $args = array(
                        'orderby' => 'count',
                        'hide_empty' => 0
                    );
                    //$skills = get_terms('job-skill', $args);
                    $term_locations = get_the_terms($post->ID, 'job-location');
                    $term_positions = get_the_terms($post->ID, 'job-position');
                    $term_skills = get_the_terms($post->ID, 'job-skill');
                    ?>
                    <?php
                    if (is_array($term_positions)) {
                        $q1 = '';
                        if (!empty($location)) {
                            $q1 = "location=" . $location->slug;
                        }
                        if (!empty($salary)) {
                            $q1 .= "&salary=" . $salary;
                        }
                        $q1 .= '&position=';                       
                        ?>
                            <?php
                            foreach ($term_positions as $position_item) {
                                ?>                
                                <a href="<?php echo bloginfo('url') . '?' . $q1 . $position_item->slug ?>" class="job-offer"><?php echo $position_item->name; ?></a>
                            <?php } ?>
                    <?php } ?>
                    
                    
                    <?php
                    if (is_array($term_skills)) {
                        ?>
                        <ul>
                            <?php
                            foreach ($term_skills as $skill_item) {
                                ?>                
                                <li><a href="<?php echo bloginfo('url') . '?' . $q . '&skill=' . $skill_item->slug ?>"><?php echo $skill_item->name ?></a></li>
                            <?php } ?>
                            <?php
                            if (is_array($term_locations)) : 
			    	$q2 = array();                               
                                if (!empty($position)) {
                                    $q2[] = "position=" . $position->slug;
                                }
                                if (!empty($salary)) {
                                    $q2[] = "&salary=" . $salary;
                                }
                                if (!empty($skill)) {
                                    $q2[] = "&skill=" . $skill->slug;
                                }
                                $q2 = implode("&", $q2);
//                                $q2 .= '&location=';    
                                foreach ($term_locations as $location) :?>
                                    <li><a href="<?php echo bloginfo('url') . '?' . $q2 . '&location=' .$location->slug ?>"><?php echo $location->name ?></a></li>
                            <?php                            
                                endforeach;
                            endif;
                            ?>
                        </ul>
                    <?php } ?>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 no-padding-lr">
                        <img class="img-responsive" src="<?php echo get_field('default_image'); ?>"> 
                        <div class="top15">
                            <?php echo get_field('job_no'); ?>
                        </div>                        
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 no-padding-lr">
                        <div class="content-job">
                            <?php if (get_field('content') !== false) :?>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding-lr">
                                <div class="heading_content">
                                事業内容 </div>
                                <i class="glyphicon glyphicon-triangle-right heading_content_icon"></i>
                                    
                            </div>
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 content_text_r ">
                                <?php echo get_field('content'); ?>
                            </div>
                            <?php endif ?>
                            
                            <?php if (get_field('detail') !== false) :?>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding-lr">                                
                                <div class="heading_content">
                                仕事内容 </div>
                                <i class="glyphicon glyphicon-triangle-right heading_content_icon"></i>
                            </div>
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 content_text_r">
                                <?php echo get_field('detail');?>
                            </div>
                            <?php endif;?>
                            
                            <?php if (get_field('experience') !== false) :?>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding-lr">                                
                                <div class="heading_content">
                                必要経験 </div>
                                <i class="glyphicon glyphicon-triangle-right heading_content_icon"></i>
                            </div>
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 content_text_r">
                                <?php echo get_field('experience'); ?>
                            </div>
                            <?php endif;?>
                            
                            <?php if (get_field('salary') !== false) :?>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding-lr">                                
                                <div class="heading_content">
                                想定給与 </div>
                                <i class="glyphicon glyphicon-triangle-right heading_content_icon"></i>
                            </div>
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 content_text_r">
                                <?php echo get_field('salary'); ?>
                            </div>
                            <?php endif;?>
                            
                            <?php if (get_field('location') !== false) :?>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding-lr">                                
                                <div class="heading_content">
                                勤務地&nbsp;&nbsp;&nbsp;&nbsp; </div>
                                <i class="glyphicon glyphicon-triangle-right heading_content_icon"></i>
                            </div>
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 content_text_r">
                                <?php echo get_field('location'); ?>                               
                            </div>
                            <?php endif;?>
                            
                            <div class="clearfix"></div>
                            <p class="viewjob"><a id="detail_job-<?php the_ID() ?>" href="<?php echo the_permalink() ?>">求人詳細を見る</a></p>
                        </div>
                    </div>                    
                </div>
            <?php endwhile; ?>
        <?php else: ?> 
            <script type="text/javascript">
                $( window ).load(function() {
                    window.location = '<?php echo home_url();?>';
                    return false;
                });
            </script>
        
        <?php endif; ?>
        <?php wp_reset_postdata() ?>


        <ul class="nav-paging">
            <?php
            if (function_exists('wp_pagenavi')) {
                wp_pagenavi();
            } else {
                ?>
                <div class="back_left"><?php next_posts_link('&laquo; Older') ?></div>
                <div class="next_right"><?php previous_posts_link('Newer &raquo;') ?></div>
            <?php } ?>        
        </ul>               
    </div>

    <?php get_template_part('part_right') ?>  
</div>
    
<script type="text/javascript">
$(document).ready(function(){
    $(".jobbox").click(function(){
        var _id = this.id;
        var href = $('#detail_job-'+_id).attr('href');
        window.location.href = href;
    });
});
</script>
<?php get_footer(); ?>