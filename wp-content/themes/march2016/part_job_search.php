
<?php
if (!defined('ABSPATH')) {
    die('No script kiddies please!');
}

$args = array(
    'post_type' => 'jobs',
    'paged' => $paged,
    'orderby' => array('date' => 'ASC')
);
$tax_query = array();
$keyword = trim($_GET['keyword']);
$position = trim($_GET['position']);
$location = trim($_GET['location']);
$skill = trim($_GET['skill']);
$salary = trim($_GET['salary']);

if (!empty($keyword)) {
    $args['s'] = $keyword;
}
if (!empty($position)) {
    $tax_query[] = array(
        'taxonomy' => 'job-position',
        'field' => 'slug',
        'terms' => $position,
    );
}
if (!empty($location)) {
    $tax_query[] = array(
        'taxonomy' => 'job-location',
        'field' => 'slug',
        'terms' => $location,
    );
}
if (!empty($skill)) {
    $tax_query[] = array(
        'taxonomy' => 'job-skill',
        'field' => 'slug',
        'terms' => $skill,
    );
}

if (count($tax_query) > 0) {
    $args['tax_query'] = $tax_query;
}

if(!empty($salary)){
    $args['meta_query'] = array(
        array(
            'key'     => 'min_salary',
            'value'   => $salary,
            'compare' => '>='
        )
    );
}
$wp_query = new WP_Query($args);
?>
<form class="form-horizontal" action="<?php bloginfo('url') ?>" method="GET">
    <div class="search-box clearfix">
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 no-padding-lr">
            <div class="input-box">
                <?php
                $plocations = get_locations();
                ?>
                <select name="location" class="form-control">
                    <option value="">--勤務地--</option>
                    <?php
                    foreach ($plocations as $arlocation) {
                        if (!empty($arlocation[1])) {
                            ?>
                            <optgroup label="<?php echo $arlocation[0]->name ?>">
                                <option value="<?php echo $arlocation[0]->slug ?>" <?php echo (isset($_GET['location']) && $_GET['location'] == $arlocation[0]->slug) ? 'selected' : '' ?>><?php echo $arlocation[0]->name ?></option>
                                <?php foreach ($arlocation[1] as $childlocation) { ?>
                                    <option value="<?php echo $childlocation->slug ?>" <?php echo (isset($_GET['location']) && $_GET['location'] == $childlocation->slug) ? 'selected' : '' ?>><?php echo $childlocation->name ?></option>
                            <?php } ?>
                            </optgroup>    
                        <?php } else { ?>
                            <option value="<?php echo $arlocation[0]->slug ?>" <?php echo (isset($_GET['location']) && $_GET['location'] == $arlocation[0]->slug) ? 'selected' : '' ?>><?php echo $arlocation[0]->name ?></option>
    <?php }
} ?>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 no-padding-lr">
            <div class="input-box">
                <?php
                $args = array(
                    'orderby' => 'count',
                    'hide_empty' => 0
                );
                $positions = get_terms('job-position', $args);
                ?>
                <select name="position" class = "form-control">
                    <option value="">-- 職種 --</option>
                    <?php foreach ($positions as $position): ?>
                        <option value="<?php echo $position->slug ?>" <?php echo (isset($_GET['position']) && $_GET['position'] == $position->slug) ? 'selected' : '' ?>><?php echo $position->name ?></option>
<?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 no-padding-lr">
            <div class="input-box">                
                <select name="salary" class = "form-control">
                    <option value="">年収下限</option>
                    <?php $salaries = array(100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000);
                    foreach($salaries as $sal){
                    ?>                    
                    <option value="<?php echo $sal ?>" <?php echo (isset($_GET['salary']) && $_GET['salary'] == $sal) ? 'selected' : '' ?>><?php echo $sal ?>万円</option>
                    <?php }?>                    
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding-lr">
            <div class="input-box">
                <input type="text" class="form-control" id="keyword" name="keyword" placeholder="フリーワードで検索" value="<?php echo isset($keyword) ? $keyword : '' ?>"></input>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 no-padding-lr">
            <div class="input-box">
                <input type="submit" value="Search">
            </div>
        </div>                    
    </div>
</form>