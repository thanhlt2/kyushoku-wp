<div class = "col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <div class="everddy-contact">
        <a href="http://agent.evolable.asia/contact-job">
            <div class="link">エージェントに<br>無料転職相談する</div>
        </a>
    </div>
    <div class = "signup-box">
        <!--h4>Sign up</h4-->
        <ul>
            <img class="img-responsive" src="<?php bloginfo('template_directory')?>/image/asenabi_banner_mailmaga.png" />
            <a href = "<?php echo bloginfo('url') . '/register' ?>"><li class = "regis">海外就職情報を受け取る</li></a>
            <a href = "#" style = "display:none"><li class = "regis-fb">Register from Facebook</li></a>
        </ul>
    </div>
    <div class = "location-box-top">
        勤務地で検索
    </div>

    <ul class = "location-list">
        <?php
        $plocations = get_locations();
        foreach ($plocations as $arlocation) {
            ?>
            <li class="item-location"><a href="<?php echo bloginfo('url') . '?location=' . $arlocation[0]->slug ?>"><?php echo $arlocation[0]->name ?></a>
                <?php if (isset($arlocation[1])) {
                    ?>
                    <ul>
                        <?php
                        foreach ($arlocation[1] as $childlocation) {
                            ?>
                            <li><a href="<?php echo bloginfo('url') . '?location=' . $childlocation->slug ?>"><?php echo $childlocation->name ?></a></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <div class="location-box-bottom"></div>       

    <?php get_template_part( 'part_right', 'news' ); ?>    
       
    <?php
    $args = array(
        'post_type' => 'banner',
        //'posts_per_page' => 10,
        'orderby' => array('date' => 'DESC'),
    );
    $loop = new WP_Query($args);
    if ($loop->have_posts()):?>
    <div class = "banner-box">
        <div class="banner-box-top">
            ピックアップ
        </div>
    
        <?php  while ($loop->have_posts()):
        $loop->the_post();
        ?>

            <ul>
                <?php
                while (have_rows('images')):
                    the_row();
                    $size = 'large';
                    $image = get_sub_field('image');
                    $alt = $image['alt'];
                    $title = $image['title'];
                    ?>
                    <a href="<?php echo get_sub_field('url') ?>" class="hovereffect"><img src="<?php echo $image['sizes'][$size] ?>" alt="<?php echo $alt ?>" title="<?php echo $title ?>" class="img-responsive" /></a><br/>
                        <?php
                    endwhile;
                    ?>
            </ul>            
        <?php  endwhile;?>
    </div>
    <?php endif;?>

</div>
