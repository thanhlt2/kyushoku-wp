<?php
/*
 * Author: haudv
 * Template Name: Register
 * 
 */
get_header();
?>
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="register-content clearfix">
            <!-- BEGIN: Benchmark Email Signup Form Code -->
            <!--script type="text/javascript" id="lbscript664801" src="https://www.benchmarkemail.com/code/lbformnew.js?mFcQnoBFKMT4JcvCuK%252F9r%252BZXUawFafLCcDZ4F%252FaIug8SQX%252Fl5TlnOA%253D%253D"></script><noscript>Please enable JavaScript <br />Powered by <a href="http://www.benchmarkemail.com" target=_new style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;">Benchmark Email</a></noscript>
            <!-- END: Benchmark Email Signup Form Code -->

            <!-- begin content left-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 regis-left clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding-lr clearfix">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 regis-l-title-l">
                        <span class="glyphicon glyphicon-envelope"></span>                    
                        <h2 class="mail-text">メルマガ会員だけの</h2>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 regis-l-title-r">
                        うれしい『特典』がいっぱい！
                    </div>  
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding-lr attr-item clearfix">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding-lr">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 first">
                            特典1
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 middle-text" style="background-color: #F08629; ">
                            <div>海外就職情報</div>
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 middle-icon no-padding-lr">
                            <span class="glyphicon glyphicon-triangle-right"></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 last attr-content">
                        <div>海外の最新求人情報をご案内します！</div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding-lr attr-item clearfix">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding-lr">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 first">
                            特典2
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 middle-text" style="background-color: #F08629; ">
                            <div>メルマガ会員限定企画</div>
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 middle-icon no-padding-lr">
                            <span class="glyphicon glyphicon-triangle-right"></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 last attr-content">
                        <div>海外起業・キャリア成功させた方の特別セミナー開催情報！</div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding-lr attr-item clearfix">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding-lr">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 first">
                            特典3
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 middle-text" style="background-color: #F08629; ">
                            <div>本気のあなたをサポート</div>
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 middle-icon no-padding-lr">
                            <span class="glyphicon glyphicon-triangle-right"></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 last attr-content">
                        <div>海外経験豊富なアドバイザーへ海外就職・転職の無料相談！</div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 notice col-lg-12 clearfix">
                    メルマガは全て無料でご覧いただけます。複数登録も可能です。</br>
                    ※個人情報について</br>
                    ご登録いただきましたメールアドレスにつきましてはアセナビの最新情報をメールでお送りする以外には使用いたしません。
                </div>               
               
            </div>
            
            
            <!-- end content left-->
            
            <!-- begin content right-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 regis-right">

                <div class="align_img">  
                    <img class="pn_image" src="<?php bloginfo('template_directory')?>/image/logo-patner.png" />
                    <div class="pn_text" >&quot;ASEANで働く&quot;を近くするウェブマガジン</div>                    
                </div>
        
                <?php echo stripcslashes($theme_options["ct_benchmark_script"]) ?>               
                
            </div>
            <!-- end content left-->

        </div>
    </div>
</div>
<div id="banner_list">
    <div class="center">
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding-lr">
            <img class="my-img" src="<?php bloginfo('template_directory')?>/image/bagan-1137015_1280.jpg" />
        </div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding-lr">
            <img class="my-img" src="<?php bloginfo('template_directory')?>/image/boot-1185675_1920.jpg" />
        </div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding-lr">
            <img class="my-img" src="<?php bloginfo('template_directory')?>/image/globe-599486_1920.jpg" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding-lr">
            <img class="my-img" src="<?php bloginfo('template_directory')?>/image/IMG_9756.JPG" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding-lr">
            <img class="my-img" src="<?php bloginfo('template_directory')?>/image/saigon-254937_1920.jpg" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding-lr">
            <img class="my-img" src="<?php bloginfo('template_directory')?>/image/singapore-1079232_1920.jpg" />
        </div>
    </div>
</div>

<div class="clearfix"></div>
<script type="text/javascript">
    $( window ).load(function() {
        resizeImageList();
    });    
    
    $(window).resize(function () {
        /* do something */ 
        resizeImageList();
    });
    function resizeImageList(){
        var width   = $( window ).width();
        var height  = width * 17/100;
        
        var myElements = document.querySelectorAll(".my-img");
 
        for (var i = 0; i < myElements.length; i++) {
            myElements[i].style.height = height+'px';
        }
    }
</script>
<?php get_footer(); ?>