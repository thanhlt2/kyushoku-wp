<?php
$args = array(
    'post_type' => 'news',
    'posts_per_page' => -1,
    'orderby' => array('date' => 'DESC'),
);
$loop = new WP_Query($args);
if ($loop->have_posts()):
    ?>
    <div class = "news-box">
        <div class="news-box-top">
            新着情報
        </div>

        <?php
        while ($loop->have_posts()):
            $loop->the_post();
            ?>

            <div class="form-inline row">
                <div class="col-xs-5 col-sm-3 col-md-6 col-lg-6">
                    <a href="<?php echo get_field('url') ?>" class="hovereffect"><img src="<?php echo get_field('image') ?>" alt="" title="<?php echo get_field('title'); ?>" class="img-responsive"/></a>
                </div>
                
                <div class="col-xs-7 col-sm-9 col-md-6 col-lg-6 padding_news_content">
                    <a href="<?php echo get_field('url') ?>"><?php echo get_field('content') ?></a>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>

