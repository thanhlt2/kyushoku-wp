<?php
/*
 * Author: haudv
 * Template Name: Index
 * 
 */
get_header();
$args = array(
    'orderby' => 'count',
    'hide_empty' => 0
);
$skills = get_terms('job-skill', $args);
$term_locations = get_the_terms($post->ID, 'job-location');
$term_positions = get_the_terms($post->ID, 'job-position');
$term_skills = get_the_terms($post->ID, 'job-skill');
$q = array();
if (count($term_locations) > 0) {
    $location = $term_locations[0];
    if($location->parent > 0){
        $parent_location = get_term_by('term_id', $location->parent, 'job-location');
        $q[] = "location=" . $parent_location->slug;
    }else{
        $q[] = "location=" . $location->slug;
    }
}
if (count($term_positions) == 1) {
    $position = $term_positions[0];
    $q[] = "position=" . $position->slug;
}
$q = implode("&", $q);
if (count($term_skills) == 1) {
    $skill = $term_skills[0];
}

$skill_names = array();
global $wp_query;
?>
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <ul class="bread-cum hidden-xs">
            <li><a href="<?php bloginfo('url') ?>">All Jobs</a></li>
            <?php if (!empty($location)) { 
                if (!empty($parent_location)) {
                ?>
                <li class="next-icon">&gt;</li>
                <li><a href="<?php echo bloginfo('url') . "?location=" . $parent_location->slug ?>"><?php echo $parent_location->name ?></a></li>
            <?php }else{ ?>
                <li class="next-icon">&gt;</li>
                <li><a href="<?php echo bloginfo('url') . "?location=" . $location->slug ?>"><?php echo $location->name ?></a></li>
            <?php }} ?>
            <?php if (!empty($position)) { ?>
                <li class="next-icon">&gt;</li>
                <li><a href="<?php echo bloginfo('url') . "?" . $q ?>"><?php echo $position->name ?></a></li>
            <?php } ?>
            <?php if (!empty($skill)) { ?>
                <li class="next-icon">&gt;</li>
                <li><a href="<?php echo bloginfo('url') . "?".$q."&skill=" . $skill->slug ?>"><?php echo $skill->name ?></a></li>
            <?php } ?>
            <li class="next-icon">&gt;</li>
            <li><?php the_title() ?></li>
        </ul>
        <?php get_template_part('part_job_search') ?>    
        <div class="toolbox">求人情報</div>
        <div class="jobbox-detail clearfix" id="<?php the_ID() ?>">
            <h1><?php the_title() ?></h1>
            <span><?php echo get_field('sub_title'); ?></span>
            <br>
            <?php
            if (is_array($term_positions)) :
                foreach ($term_positions as $position_item) :
            ?>
            <a href="<?php echo bloginfo('url') . '?position=' . $position_item->slug ?>" class="job-offer"><?php echo $position_item->name; ?></a>
            <?php
                endforeach;
            endif;
            ?>
            <?php
            if (is_array($term_skills)) {
                ?>
                <ul>
                    <?php
                    foreach ($term_skills as $skill) {
                        ?>                
                        <li><a href="<?php echo bloginfo('url') . "?skill=" . $skill->slug ?>"><?php echo $skill->name ?></a></li>
                    <?php } ?>
                        
                    <?php
                        if (is_array($term_locations)) :
                            foreach ($term_locations as $location) :?>
                                <li><a href="<?php echo bloginfo('url') . '?location=' .$location->slug ?>"><?php echo $location->name ?></a></li>
                        <?php                            
                            endforeach;
                        endif;
                        ?>
                </ul>
            <?php } ?>            
                        
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding-lr top15">
                <img class="img-responsive img-main" src="<?php echo get_field('detail_image'); ?>">               
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding-lr top15">
                <div class="content-job-1">
                    <?php if (get_field('description') !== false) :?>                    
                        <?php echo get_field('description'); ?>
                    <?php endif ?>
                </div>
            </div>  
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding-lr">
                <div class="content-job-1">     
                    <?php
                    $job_no = "-1";
                    $displayed = false;
                    if (have_rows('block_info')):
                        ?>
                        <?php
                        while (have_rows('block_info')): the_row();
                            $tname = "description_1";
                            $title = get_sub_field('title');
                            if ($title == '企業・求人概要') {
                                $tname = "description_1";
                            } else if ($title == '応募条件') {
                                $tname = "description_2";
                            } else if ($title == '勤務・就業規定・その他情報') {
                                $tname = "description_3";
                            } else if ($title == '募集要項') {
                                $tname = "description_4";
                            }
                            ?>
                            <h2><?php echo get_sub_field('title') ?></h2>
                            <div class="table-content">
                                <?php while (have_rows($tname)): the_row() ?>
                                    <?php
                                    if ($title == '募集要項' && !$displayed):
                                        ?>
                                        <div class="row-content">
                                            <div class="cell-content-1">求人No</div>
                                            <div class="cell-content-2">
                                                <?php
                                                $job_no = get_field('job_no');
                                                echo get_field('job_no');
                                                $displayed = TRUE;
                                                ?>
                                            </div>
                                        </div>
            <?php endif; ?>
                                    <div class="row-content">
                                        <div class="cell-content-1"><?php echo get_sub_field('sub_title') ?></div>
                                        <div class="cell-content-2">
            <?php echo get_sub_field('content') ?>
                                        </div>
                                    </div>
                            <?php endwhile; ?>                                                                                 
                            </div>
                        <?php endwhile; ?>
<?php endif; ?>
                    <p class="viewjob"><a href="<?php echo bloginfo('url') . '/job-apply/' . get_the_ID() ?>">応募フォーム</a></p>
                </div>
            </div>
        </div>

    </div>
    <?php get_template_part('part_right') ?>  
</div>
<?php get_footer(); ?>