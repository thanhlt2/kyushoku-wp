$().ready(function () {    
    jQuery.validator.addMethod("phone", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.match(/^[0-9][0-9\-]+$/);
    }, "Invalid phone number");    
        
    var error_icon_template = '';


    $('#apply-form').validate({
        rules: {
            'email': {
                required: true,
                email: true
            },
            'canname': {
                required: true
            },
            'phone_number': {
                required: true,
                phone:true
            },
            'phonetic_name': {
                required: true
            }
        },
        messages: {
            'email': error_icon_template,
            'canname': error_icon_template,
            'phone_number': error_icon_template,
            'phonetic_name': error_icon_template
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});