<?php
/*
 * Author: haudv
 * Template Name: Job Apply
 * 
 */
get_header();
global $wp;
$req = explode('/', $wp->request);
$job_id = $req[1];
$job = get_post($job_id);
$cjob = get_post_custom($job_id);

$ar_position = get_terms('job-position', array('slug'=>trim($_GET['position'])));
$position = count($ar_position)==1?$ar_position[0]:null;
$ar_location = get_terms('job-location', array('slug'=>trim($_GET['location'])));
$location = count($ar_location)==1?$ar_location[0]:null;
if(isset($location) && $location->parent > 0){
   $parent_location = get_term_by('term_id', $location->parent, 'job-location');
}
$ar_skill = get_terms('job-skill', array('slug'=>trim($_GET['skill'])));
$skill = count($ar_skill)==1?$ar_skill[0]:null;
$salary = trim($_GET['salary']);
$q = array();
if (!empty($location)) {
    $q[] = "location=" . $location->slug;
}
if (!empty($position)) {
    $q[] = "position=" . $position->slug;
}
if (!empty($salary)) {
    $q[] = "salary=" . $salary;
}
$q = implode("&", $q);
$args = array(
    'orderby' => 'count',
    'hide_empty' => 0
);
?>
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="apply-content clearfix">
            <h2>応募する求人</h2>
            <h1><?php echo $job->post_title?></h1>
            <span><?php echo isset($cjob['sub_title'][0])?$cjob['sub_title'][0]:'';?></span>
            <br>
            <?php
            $args = array(
                'orderby' => 'count',
                'hide_empty' => 0
            );
            //$skills = get_terms('job-skill', $args);
            $term_locations = get_the_terms($job_id, 'job-location');
            $term_positions = get_the_terms($job_id, 'job-position');
            $term_skills = get_the_terms($job_id, 'job-skill');
            ?>
            <?php
            if (is_array($term_positions)) {
                if (!empty($location)) {
                    $q1 = "location=" . $location->slug;
                }
                if (!empty($salary)) {
                    $q1 .= "salary=" . $salary;
                }
                $q1 .= '&position=';
                ?>
                    <?php
                    foreach ($term_positions as $position) {
                        ?>                
                        <a href="<?php echo bloginfo('url') . '?' . $q1 . $position->slug ?>" class="job-offer"><?php echo $position->name; ?></a>
                    <?php } ?>
            <?php } ?>

            <?php
            if (is_array($term_skills)) {
                ?>
                <ul>
                    <?php
                    foreach ($term_skills as $skill) {
                        ?>                
                        <li><a href="<?php echo bloginfo('url') . '?' . $q . '&skill=' . $skill->slug ?>"><?php echo $skill->name ?></a></li>
                    <?php } ?>
                </ul>
            <?php } ?>
           
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 no-padding-lr">
                 <?php if(isset($cjob['default_image'][0])) :
                    $defaultImage = wp_get_attachment_image_src($cjob['default_image'][0], 'full' );
                ?>
                <img class="img-responsive" src="<?php echo isset($defaultImage[0])?$defaultImage[0]:''; ?>">    
                <?php                
                endif;
                ?>
            </div>
            

            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 no-padding-lr">
                <div class="content-job">

                    <?php if (isset($cjob['content'][0])) :?>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding-lr">
                        <div class="heading_content">
                        事業内容 </div>
                        <i class="glyphicon glyphicon-triangle-right heading_content_icon"></i>

                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 content_text_r ">
                        <?php echo $cjob['content'][0]; ?>
                    </div>
                    <?php endif ?>

                    <?php if (isset($cjob['detail'][0])) :?>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding-lr">                                
                        <div class="heading_content">
                        仕事内容 </div>
                        <i class="glyphicon glyphicon-triangle-right heading_content_icon"></i>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 content_text_r">
                        <?php echo $cjob['detail'][0];?>
                    </div>
                    <?php endif;?>

                    <?php if (isset($cjob['experience'][0])) :?>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding-lr">                                
                        <div class="heading_content">
                        必要経験 </div>
                        <i class="glyphicon glyphicon-triangle-right heading_content_icon"></i>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 content_text_r">
                        <?php echo $cjob['experience'][0]; ?>
                    </div>
                    <?php endif;?>

                    <?php if (isset($cjob['salary'][0])) :?>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding-lr">                                
                        <div class="heading_content">
                        想定給与 </div>
                        <i class="glyphicon glyphicon-triangle-right heading_content_icon"></i>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 content_text_r">
                        <?php echo $cjob['salary'][0]; ?>
                    </div>
                    <?php endif;?>

                    <?php if (isset($cjob['location'][0])) :?>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding-lr">                                
                        <div class="heading_content">
                        勤務地&nbsp;&nbsp;&nbsp;&nbsp; </div>
                        <i class="glyphicon glyphicon-triangle-right heading_content_icon"></i>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 content_text_r">
                        <?php echo $cjob['location'][0]; ?>                               
                    </div>
                    <?php endif;?>

                    <div class="clearfix"></div>
                </div>
            </div>            

            <div class="clearfix"></div>
            <h2>お申し込みフォーム</h2>
            <form name="apply-form" id="apply-form" class="apply-form form-horizontal" method="post" action="<?php bloginfo('url') ?>/job-confirm" enctype="multipart/form-data">
            <div class="apply-content-table-border">
                <div class="apply-content-table">                            
                    <div class="apply-content-row">
                        <div class="apply-content-cell-1">
                            <h4>お名前/NAME</h4>
                            <span>[必須]</span>
                        </div>
                        <div class="apply-content-cell-2">
                            <input type="text" name="canname" value="<?php echo isset($_SESSION['candidate']['name'])?$_SESSION['candidate']['name']:''?>">
                            <p>（例）求職　太郎／John Smith</p>
                        </div>
                    </div>
                    <div class="apply-content-row">
                        <div class="apply-content-cell-1">
                            <h4>氏名(フリガナ).</h4>
                            <span>[必須]</span>
                        </div>
                        <div class="apply-content-cell-2">
                            <input type="text" name="phonetic_name" id="phonetic_name" value="<?php echo isset($_SESSION['candidate']['phonetic_name'])?$_SESSION['candidate']['phonetic_name']:''?>">
                            <p>（例）キュウショク　タロウ・ジョン　スミス</p>
                        </div>
                    </div> 
                    <div class="apply-content-row">
                        <div class="apply-content-cell-1">
                            <h4>電話番号／Phone Number</h4>
                            <span>[必須]</span>
                        </div>
                        <div class="apply-content-cell-2">
                            <input type="text" name="phone_number" id="phone_number" value="<?php echo isset($_SESSION['candidate']['phone_number'])?$_SESSION['candidate']['phone_number']:''?>">
                            <p>（例）090-1234-5678</p>
                        </div>
                    </div> 
                    <div class="apply-content-row">
                        <div class="apply-content-cell-1">
                            <h4>メールアドレス／e-mail address</h4>
                            <span>[必須]</span>
                        </div>
                        <div class="apply-content-cell-2">
                            <input type="text" name="email" id="email" value="<?php echo isset($_SESSION['candidate']['email'])?$_SESSION['candidate']['email']:''?>">
                            <p>（例）info@agent.evolable.asia</p>
                        </div>
                    </div>  
                    <div class="apply-content-row">
                        <div class="apply-content-cell-1">
                            <h4>履歴書</h4>
                        </div>
                        <div class="apply-content-cell-2">
                            <input type="file" name="resume_file" id="resume_file">
                            <?php if(isset($_SESSION['file_errors']['resume_file'])){?>
                            <label id="resume_file-error" class="error" for="resume_file"><?php echo $_SESSION['file_errors']['resume_file']?></label>
                            <?php }?>
                            <p>ファイルを選択 履歴書ファイルを添付してください</p>
                        </div>
                    </div>   
                    <div class="apply-content-row">
                        <div class="apply-content-cell-1">
                            <h4>職務経歴書</h4>
                        </div>
                        <div class="apply-content-cell-2">
                            <input type="file" name="cv_file" id="cv_file">
                            <?php if(isset($_SESSION['file_errors']['cv_file'])){?>
                            <label id="cv_file-error" class="error" for="cv_file"><?php echo $_SESSION['file_errors']['cv_file']?></label>
                            <?php }?>
                            <p>ファイルを選択 職務経歴書ファイルを添付してください</p>
                        </div>
                    </div>
                    <div class="apply-content-row">
                        <div class="apply-content-cell-1">
                            <h4>その他 添付ファイル</h4>
                        </div>
                        <div class="apply-content-cell-2">
                            <input type="file" name="other_file" id="other_file">
                            <?php if(isset($_SESSION['file_errors']['other_file'])){?>
                            <label id="other_file-error" class="error" for="other_file"><?php echo $_SESSION['file_errors']['other_file']?></label>
                            <?php }?>
                            <p>その他のファイルはこちらに添付してください</p>
                        </div>
                    </div>
                    <div class="apply-content-row">
                        <div class="apply-content-cell-1">
                            <h4>希望年収 [万円]</h4>
                        </div>
                        <div class="apply-content-cell-2">
                            <input type="text"  name="expected_salary" id="expected_salary">
                            <p>お仕事紹介をご希望される方はご記入ください</p>
                        </div>
                    </div>
                    <div class="apply-content-row">
                        <div class="apply-content-cell-1">
                            <h4>その他ご要望.</h4>
                        </div>
                        <div class="apply-content-cell-2">
                            <textarea rows="2" name="request" id="request"></textarea>
                            <p>ご希望の職種、その他お問い合わせ等ございましたらこちらにご記入ください。</p>
                        </div>
                    </div>     
                    <div class="apply-content-row">
                        <div class="apply-content-cell-1">
                            <h4>どこで求人を知りましたか?</h4>
                        </div>
                        <div class="apply-content-cell-2">
                            <div class="checkbox">
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="inq[]" value="エバディHPから" <?php echo $arr_inq_checked[0] ?>/> エバディHPから
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="inq[]" value="検索エンジンで検索" <?php echo $arr_inq_checked[1] ?>/> 検索エンジンで検索
                                </label>                                
                            </div>
                            <div class="checkbox">
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="inq[]" value="Twitter・Facebookなどのソーシャルネットワークから" <?php echo $arr_inq_checked[2] ?>/> Twitter・Facebookなどのソーシャルネットワークから
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="inq[]" value="海外就職情報サイトから <?php echo $arr_inq_checked[3] ?>"/> 海外就職情報サイトから
                                </label>
                            </div>
                            <div class="checkbox">
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="inq[]" value="アセナビから" <?php echo $arr_inq_checked[4] ?>/> アセナビから
                                </label>
                            </div>
                            <div class="checkbox">
                                <label class="other">
                                    <input type="checkbox" id="inq_o" name="inq[]" for="contact-inq_other_text" value="その他" <?php echo $arr_inq_checked[5] ?>/> その他
                                    <input type="text" id="contact-inq_other_text" name="inq_other" class="form-control" style="color: #000;" <?php echo ($arr_inq_checked[5] == 'checked') ? '' : 'disabled="disabled"' ?> value="<?php echo $reg_inq_other ?>" />
                                </label>
                            </div>
                        </div>
                    </div> 
                </div>                        
            </div>
            <div class="rule">
                監査責任者を選任し、個人情報の保護に関する実践と運用状況の内部監査を実施します。</br>
                お取引先の企業および個人に対し、個人情報の保護に係わる協力を要請します。</br>
                基本方針は、当社のホームページ（http://agent.evolable.asia/）に掲載することにより、いつでもどなたにも閲覧可能な状態とします。個人情報に関するお問い合わせにつきましてもホームページ掲載の窓口にて受付いたします。</br>
                個人情報保護マネジメントシステムは適宜見直しをし、継続的に改善します。</br>
                1.個人情報の取得および利用について</br>
                個人情報のご提供をお願いする際には、あらかじめその利用目的を明示いたします。 また、ご提供頂きました個人情報は、以下に記載する利用目的の範囲内で利用し、皆様の同意なく利用目的以外に使用することはありません。</br>

                万一、当該目的以外の目的で利用する場合や、利用目的そのものを変更する場合は事前に速やかに告知いたします。</br>

                また利用目的に照らして不要となった個人情報については速やかに且つ適正に削除・廃棄いたします。</br>

                個人情報の種類 利用目的 </br>
                新規の有料職業紹介事業者の情報 当社で開催するセミナー及び当社サービスのご案内のため。 </br>
                メールマガジンの配信情報 当社で開催するセミナー及び当社サービスのご案内のため。 </br>
                一般に公開されているHPなどの情報ならびに書籍の情報 当社で開催するセミナー及び当社サービスのご案内のため。 </br>
                当社で開催するセミナーへのお申込み、アンケートでご提出いただいた情報 次回以降のセミナーのご案内のため。当社商品・サービスの評価のため。その他、個別にご同意いただいた利用目的のため。 </br>
                お客様に関する情報 お客様管理、営業活動、各種ご連絡、季節のご挨拶状送付のため。 </br>
                サポートへお問合せ頂いた情報 お問い合わせへのご回答のため。 </br>
                他の事業者から個人情報の処理の全部または一部について委託された場合等（データ移行等） 委託された当該業務の利用目的の範囲内において適切且つ円滑な遂行のため。 </br>
                制作会社様・転職ポータルサイト様の情報 商談上のご連絡のため。 </br>
                採用応募者の情報 選考ならびに採用希望者へのご連絡のため。 </br>
                当社従業員、その家族または退職者を含む関係者等の情報 当社従業員の労務管理のため。 </br>



                2.個人情報の第三者への開示・提供について</br>
                当社は、以下のいずれかに該当する場合を除き、個人情報を第三者へ開示または提供しません。 </br>

                ご本人の同意がある場合</br>
                個人情報の取扱に関する業務の全部または一部を委託する場合</br>
                （但しこの場合、当社は委託先との間で個人情報保護に関する契約を締結する等、委託先の適切な監督に努めます。）</br>
                統計的なデータなどご本人を識別することができない状態で開示・提供する場合</br>
                法令に基づき開示・提供を求められた場合</br>
                人の生命、身体または財産の保護のために必要な場合であって、ご本人の同意を得ることが困難である場合</br>
                公衆衛生の向上又は児童の健全な育成の推進のために特に必要がある場合であって、本人の同意を得ることが困難であるとき</br>
                国または地方公共団体等が公的な事務を実施するうえで、協力する必要がある場合であって、ご本人の同意を得ることにより当該事務の遂行に支障を及ぼすおそれがある場合</br>
                3.個人情報の共同利用について</br>
                当社は、他社と合同セミナーを開催する場合に、個人情報を共同利用することがあります。この場合には、予め利用目的等を通知又は公表いたします。また、安全管理が図られるよう、個人情報の共同利用に関する契約の締結や適切な管理を行います。</br>
                </br>
                4.個人情報の取り扱いに関する委託について</br>
                当社は、利用目的の達成に必要な範囲内において、他の事業者へ個人情報を委託することがあります。この場合には、個人情報保護体制が整備された委託先を選定するとともに、個人情報保護に関する契約を締結いたします。</br>


                5.個人情報に関する権利（開示等、訂正等、利用停止等の請求）のお問合せ</br>
                1.に該当する個人情報に関して、皆様がご自身の情報の開示、訂正、追加、削除ならびに利用停止・消去（以下、開示等）をご希望される場合には、お申し出いただいた方がご本人であることを確認したうえで迅速に対応させて頂きます。</br>

                また、以下に記載する場合は開示等の対象外とさせていただきます。</br>

                開示等を行わないことを決定した場合は、その旨理由を付して通知いたします。</br>

                【全般】</br>

                ご本人であることが確認できない場合</br>
            </div>
            <p class="applyjob"><button type="submit">エントリー</button></p>
            <input type="hidden" name="job_id" value="<?php echo $job_id?>"/>
            <input type="hidden" name="confirm" value="confirm"/>
            </form>
        </div>
    </div>
</div>
<?php get_footer(); ?>
<script>
$(function(){
    $('#inq_o').on('click', function(){
        if($(this).is(':checked')){
            $('#contact-inq_other_text').prop('disabled', false);
        }else {
            $('#contact-inq_other_text').prop('disabled', true);
        }
    });
});
</script>
